package com.im.test

import spock.lang.Specification
import spock.lang.Unroll
import spock.util.mop.ConfineMetaClassChanges

import java.text.SimpleDateFormat

class UserSpec extends Specification {
/*
    @Unroll
    def "test for get full user full name"() {

        given: "get user info"
        User user = new User(firstName: "${firstName}", lastName: "${lastName}")

        expect: "check the result"
        user.getFullName() == result

        where:
        firstName | lastName | result
        "arun"    | "tanwar" | "arun tanwar"


    }

    @Unroll
    def "test for dislay fullname with gender"() {
        given:
        User user = new User(firstName: "${firstName}", lastName: "${lastName}", gender: "{${gender}")

        expect:
        user.displayName() == result

        where:
        firstName | lastName | gender   | result
        "varun"   | "tanwar" | "Female" | "Msvarun tanwar"

    }


    def "test for valid passward"() {
        given:
        User userp = new User()

        expect:
        userp.isValidPassword(password) == result

        where:
        password   | result
        "test"     | false
        "bcjdbchd" | true
    }

    @Unroll
    @ConfineMetaClassChanges([User])
    def "test resetPasswordAndSendEmail"() {

        given:
        User user = new User()

        and:
//        String content=null
//        String cont
//        def mockedPasswordEncrypterService= Mock(PasswordEncrypterService)
//        cont=mockedPasswordEncrypterService.encrypt(content)
//        def mockedEmailService=Mock(EmailService)
//        mockedEmailService.sendCancellationEmail(user, content)

        and: "Mocking response for encyryptPassword"
        User.metaClass.encyryptPassword = { String pwd ->
            return "EncryptedPassword"
        }

        and: "Mocking response for emailService.sendCancellationEmail"
        EmailService mockedEmailService = Mock(EmailService)
        user.emailService = mockedEmailService
        mockedEmailService.sendCancellationEmail(_) >> [println("mocked sendCancellationEmail")]

        when: "calling resetPasswordAndSendEmail"
        user.resetPasswordAndSendEmail()

        then: "validating pass condition"
        //todo check for a valid condition
        assert true

    }


    @Unroll
    @ConfineMetaClassChanges([User])
    def "test for encryptpassword"() {

        given:
        User user = new User();


        and: "mocking response for isvalidpassword"
        User.metaClass.isValidPassword = { String pwd ->
            println "Mock"
            return true
        }

        and: "mocking response for passwordEncrypterService"
        def passwordEncrypterService = Mock(PasswordEncrypterService)
        user.passwordEncrypterService = passwordEncrypterService
        passwordEncrypterService.encrypt(_ as String) >> "aruntanwar"



        expect: "calling encryptpassword"
        user.encyryptPassword("password") == "aruntanwar"


    }


    def "getincomegroup"() {
        given:

        User user = new User()
        user.incomePerMonth = income

        expect:
        user.getIncomeGroup() == result1;

        where:
        income | result1
        5000   | "MiddleClass"


    }


    def "test for purchase"() {

        given:
        Product p = new Product()
        User user = new User();
        user.purchase(p)
        expect:
        user.purchasedProducts.size() == 1


    }


    def "test for cancel purchase"() {
        given:
        Product p = new Product()
        User user = new User()
        user.cancelPurchase(p)

        expect:
        user.purchasedProducts.size() == 0
    }

    def "test the interseted catoriges"() {

        given:
        User user = new User();

        when:
        def start = new Date();
        user.getInterestedInCategories()

        then:
        def end = new Date();
        long difference = end.getTime() - start.getTime();
        difference >= 10000
        println difference;
    }


*/

}
