package com.im.test

import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import spock.util.mop.ConfineMetaClassChanges

class TransactionSpec extends Specification {

    @Shared Transaction transaction = new Transaction()



    def "test the sell"(){
        given:
        Product product = new Product(price: 100)
        User user = new User(balance: 200)

        when:
        transaction.sell(product, user)

        then:
        user.balance ==  100.toBigDecimal()
    }



    def "2 time test the sell"(){
        given:
        Product product = new Product(price: 200)
        User user = new User(balance: 100)

        when:
        transaction.sell(product, user)

        then:
        def e=thrown(SaleException)
        e.message=="Not enough account balance"
    }


    def "test for calculate discount"()
    {
        given:
        Product product =new Product(name:"arun" , price: 100,discountType: DiscountType.ALL)
        User user=new User(balance: 300)

        when:
        def discount =transaction.calculateDiscount(product,user)

        then:
        discount==10.toBigDecimal()

    }
    @ConfineMetaClassChanges([Transaction])
    def "testfor cancelsale"()
    {
        given:
        Product product =new Product(name:"arun" , price: 100,discountType: DiscountType.ALL)
        User user=new User(balance: 300)
        System.err.println("user works")

        and:
        transaction.metaClass.calculateDiscount ={Product product1,User user1 -> return 30}





        when:
        transaction.cancelSale(product,user)

        then:
        user.balance==370;



    }






}
